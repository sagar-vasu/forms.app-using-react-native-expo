import React from 'react';
import { Text, StyleSheet, TouchableOpacity, View } from 'react-native';

class Card extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity style={styles.button} activeOpacity={0.8} onPress={this.props.onPress} >
                    <Text style={styles.text}>
                        {this.props.name}
                    </Text>
                </TouchableOpacity>

            </View>
        )
    }
}



// styles

const styles = StyleSheet.create({
    container: {
        marginTop: 20
    },
    button: {
        width: 266,
        borderRadius: 2,
        backgroundColor: '#1E4366',
        padding: 10,
    },
    text: {
        fontSize: 20,
        color: 'white',
        textAlign: 'center',
        fontWeight: 'bold'
    }
})



export default Card 