import React, { Component } from 'react'
import { StyleSheet, View, Text, Picker, Dimensions } from 'react-native'

const deviceWidth = Dimensions.get('window').width;


export default class SwitchExample extends Component {
    constructor() {
        super()
        this.state = {
            component: "Picker Component",
            choosenIndex: 0
        }
    }


    // handelChange
    handelChange = (itemValue, itemPosition) => {
        this.setState({ language: itemValue, choosenIndex: itemPosition })
    }


    render() {
        return (
            <View style={styles.container}>
                <Picker style={styles.pickerStyle}
                    selectedValue={this.state.language}
                    onValueChange={this.handelChange}
                >
                    {
                        this.props.items && this.props.items.map((v, i) => {
                            return <Picker.Item key={i} label={v.label} value={v.value} />
                        })
                    }
                </Picker>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        width: deviceWidth - 30,
        borderWidth: 1,
        borderColor: '#DADADA',
        margin: 10
    },
    textStyle: {
        margin: 24,
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    pickerStyle: {
        width: "100%",
        color: '#344953',
        justifyContent: 'center',
        height: 38



    }
})  