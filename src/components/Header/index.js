import React, { Component } from 'react';
import { Header, Left, Body, Button, Title } from 'native-base';

import { StyleSheet } from 'react-native'


export default class HeaderIconExample extends Component {
    render() {
        return (
            <Header style={styles.header}>
                <Left>
                    <Button transparent onPress={this.props.onClick}>
                        <Title style={[styles.text, { color: '#007AFF' }]}>Cancel</Title>
                    </Button>
                </Left>
                <Body>
                    <Title style={[styles.text, { color: '#000000' }]}>Farm Search</Title>
                </Body>

            </Header>
        );
    }
}


const styles = StyleSheet.create({
    header: {
        backgroundColor: "#F8F8F8"
    },
    text: {
        fontSize: 17,
        fontWeight: '400'
    }
})