import React, { Component } from 'react';
import { Input, Item, View } from 'native-base';
import { StyleSheet } from 'react-native'

class RegularInput extends Component {
    render() {
        return (
            <View style={styles.inputContainer}>
                <Item regular style={styles.input}>
                    <Input placeholder={this.props.placeholder} type={this.props.inputType} />
                </Item>
            </View>
        );
    }
}

// styles

const styles = StyleSheet.create({

    input: {
        width: '90%',
        alignSelf: 'center',
        marginTop: 25,
        height: 40
    }
})



export default RegularInput 