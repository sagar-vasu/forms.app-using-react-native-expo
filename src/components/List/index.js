import React, { Component } from 'react';

// library
import { List, ListItem, Left, Body, Text } from 'native-base';
import { StyleSheet } from 'react-native'

// icons
import { MaterialCommunityIcons } from '@expo/vector-icons';

export default class ListAvatarExample extends Component {
    constructor() {
        super()
        this.state = {
            component: "List Item"
        }
    }
    render() {
        return (
            <List>
                <ListItem avatar>
                    <Left>
                        <MaterialCommunityIcons name={this.props.iconName} size={32} color="#1E4366" />
                    </Left>
                    <Body>
                        <Text style={styles.propertiesText}>{this.props.title}</Text>
                        <Text note style={styles.addedProperty}>{this.props.description}</Text>
                    </Body>
                </ListItem>
            </List>
        );
    }
}



const styles = StyleSheet.create({
    propertiesText: {
        color: '#007AFF',
        fontWeight: 'bold'
    },
    addedProperty: {
        fontWeight: 'bold'
    },
})