import React from 'react';
import { Text, StyleSheet, TouchableOpacity } from 'react-native';

class Button extends React.Component {
    render() {
        return (
            <TouchableOpacity style={styles.button} activeOpacity={0.8} onPress={this.props.onPress} >
                <Text style={styles.text}>
                    {this.props.name}
                </Text>
            </TouchableOpacity>
        )
    }
}



// styles

const styles = StyleSheet.create({
    button: {
        width: 266,
        borderRadius: 2,
        backgroundColor: '#1E4366',
        padding: 20,
    },
    text: {
        fontSize: 20,
        color: 'white',
        textAlign: 'center',
        fontWeight: 'bold'
    }
})



export default Button 