import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

// our all app screens
import {
    SyncingScreen,
    SyncingResultScreen,
    Dashboard,
    FormSearch,
    FormSearchLocation,
    AboutScreen,
    Setting
} from '../../screens'

const AppNavigator = createStackNavigator({

    SyncLoading: {
        screen: SyncingScreen,
        navigationOptions: {
            headerShown: false,
        }

    },
    SyncResult: {
        screen: SyncingResultScreen,
        navigationOptions: {
            title: 'Back',
            headerTitleStyle: { color: "#007AFF", fontSize: 17, fontWeight: '300' },
            headerTintColor: "#007AFF"

        }
    },
    Dashboard: {
        screen: Dashboard,
        navigationOptions: {
            headerShown: false,
        }
    },
    FormSearch: {
        screen: FormSearch,
        navigationOptions: {
            headerShown: false,
        }
    },
    FormSearchLocation: {
        screen: FormSearchLocation,
        navigationOptions: {
            headerShown: false,
        }
    },

    About: {
        screen: AboutScreen,
        navigationOptions: {
            title: 'Back',
            headerTitleStyle: { color: "#007AFF", fontSize: 17, fontWeight: '300' },
            headerTintColor: "#007AFF"
        }
    },
    Setting: {
        screen: Setting,
        navigationOptions: {
            title: 'Back',
            headerTitleStyle: { color: "#007AFF", fontSize: 17, fontWeight: '300' },
            headerTintColor: "#007AFF"
        }
    }

}, {
    initialRouteName: "SyncLoading"
});

export default createAppContainer(AppNavigator)