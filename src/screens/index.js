// our all screen components

import SplashScreen from "./Splash-Screen";
import SyncingScreen from "./Syncing-Screen";
import SyncingResultScreen from './Syncing-Result-Screen';
import Dashboard from './Dashboard';
import FormSearch from './Form-Search-Screen';
import FormSearchLocation from './Form-Search-Location-Screen';
import AboutScreen from './About';
import Setting from './Settings-Screen'




export {
    SplashScreen,
    SyncingScreen,
    SyncingResultScreen,
    Dashboard,
    FormSearch,
    FormSearchLocation,
    AboutScreen,
    Setting
}