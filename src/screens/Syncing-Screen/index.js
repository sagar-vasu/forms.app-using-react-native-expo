import React from 'react';
import { View, Text, StyleSheet, SafeAreaView, Image } from 'react-native';

// components
import { Loader, Button } from '../../components'

class SyncingScreen extends React.Component {

    constructor() {
        super()
        this.state = {
            screen: "Syncing Loading Screen"
        }
    }


    // handleClick
    handleClick = () => {
        this.props.navigation.navigate("SyncResult")
    }



    render() {
        return (
            <SafeAreaView style={styles.safeView}>
                <View style={styles.container}>
                    <View style={styles.logoContainer}>
                        <Image
                            source={require("../../../assets/images/header.png")}
                        />
                    </View>
                    <View style={styles.loadingContainer}>
                        <Text style={styles.text}>Syncing</Text>
                        <View style={styles.loader}>
                            <Loader />
                        </View>
                    </View>

                    <View style={styles.buttonContainer}>
                        <Button name="SYNC" onPress={this.handleClick} />
                    </View>
                </View>

            </SafeAreaView>
        )
    }
}



// styles

const styles = StyleSheet.create({
    safeView: {
        flex: 1,
        paddingTop: 24

    },
    container: {
        flex: 1,
    },
    logoContainer: {
        marginTop: 30,
        alignItems: 'center'
    },
    loadingContainer: {
        marginTop: 70,
        alignItems: 'center'
    },
    text: {
        fontSize: 33,
        color: '#000000'
    },
    loader: {
        marginTop: 50
    },
    buttonContainer: {
        marginTop: 100,
        alignItems: 'center'
    }
})



export default SyncingScreen