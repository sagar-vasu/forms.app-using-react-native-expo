import React from 'react';
import { View, Image, Text, StyleSheet, SafeAreaView } from 'react-native';


class AboutScreen extends React.Component {
    constructor() {
        super()
        this.state = {
            screen: "About Screen"
        }
    }


    // handleClick
    handleClick = () => {
        this.props.navigation.navigate("Dashboard")
    }

    render() {
        return (
            <SafeAreaView style={styles.safeView}>
                <View style={styles.logoContainer}>
                    <Image
                        source={require("../../../assets/images/header.png")}
                    />
                </View>
                <View style={styles.formTitleContainer}>
                    <Text style={styles.formTitle}>Farms.ca app v1.0</Text>
                </View>

                <View style={styles.formDescriptionContainer}>


                    <Text style={styles.formDescription}>Initial release notes</Text>
                    <Text style={styles.formDescription}>• List management</Text>
                    <Text style={styles.formDescription}>• Photo management</Text>
                    <Text style={styles.formDescription}>• Add notification settings</Text>
                </View>



            </SafeAreaView>
        )
    }
}


// styles

const styles = StyleSheet.create({
    safeView: {
        flex: 1,
        paddingTop: 24

    },
    container: {
        flex: 1,
    },
    logoContainer: {
        marginTop: 70,
        alignItems: 'center'
    },
    formTitleContainer: {
        marginTop: 70
    },
    formTitle: {
        fontSize: 28,
        fontWeight: 'bold',
        color: '#1E4366',
        alignSelf: 'center',
    },
    formDescriptionContainer: {
        marginTop: 70,
    },
    formDescription: {
        fontWeight: 'bold',
        alignSelf: 'center'
    }



})


export default AboutScreen