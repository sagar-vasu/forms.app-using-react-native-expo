import React from 'react';
import { View, Dimensions, StyleSheet, ImageBackground, Image, SafeAreaView } from 'react-native';

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

class SplashScreen extends React.Component {
    constructor() {
        super()
        this.state = {
            screen: "Splash"
        }
    }

    // component did mount
    // componentDidMount() {

    //     setTimeout(() => {
    //         this.props.navigation.navigate("SyncLoading");
    //     }, 2000);
    // }
    render() {
        return (
            <SafeAreaView style={styles.safeView}>
                <View style={styles.container}>
                    <ImageBackground
                        source={require("../../../assets/images/splash.png")}
                        style={styles.backgroundImage}
                    >
                        <Image
                            source={require("../../../assets/images/state.png")}
                            style={styles.stateImage}
                        />
                    </ImageBackground>

                    <View style={styles.logoContainer}>
                        <Image
                            source={require("../../../assets/images/header.png")}
                        />
                    </View>

                </View>
            </SafeAreaView>
        )
    }
}



// styles

const styles = StyleSheet.create({
    safeView: {
        flex: 1,
        paddingTop: 24

    },
    container: {
        flex: 1,
    },
    backgroundImage: {
        width: deviceWidth,
        height: deviceHeight - 150
    },
    stateImage: {
        alignSelf: 'center',
        marginTop: 30
    },
    logoContainer: {
        marginTop: 20,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})



export default SplashScreen

