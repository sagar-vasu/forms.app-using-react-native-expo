import React from 'react';
import { View, Text, StyleSheet, SafeAreaView, Switch } from 'react-native';



// native base
import { ListItem, Body, Right, } from 'native-base'


class SettingScreen extends React.Component {
    constructor() {
        super()
        this.state = {
            screen: "Setting Screen",
            automaticSync: false,
            enableSync: false
        }
    }


    // handleClick
    handleClick = () => {
        this.props.navigation.navigate("Dashboard")
    }

    render() {
        return (
            <SafeAreaView style={styles.safeView}>

                <View style={styles.bodyContainer}>
                    <Text style={styles.text}>Sync Settings</Text>

                    <View>
                        <ListItem icon>
                            <Body>
                                <Text>Enable Sync</Text>
                            </Body>
                            <Right>
                                <Switch
                                    onValueChange={() => this.setState({ enableSync: !this.state.enableSync })}
                                    value={this.state.enableSync}
                                />
                            </Right>
                        </ListItem>
                        <ListItem icon>
                            <Body>
                                <Text>Automatic Sync</Text>
                            </Body>
                            <Right>
                                <Switch
                                    onValueChange={() => this.setState({ automaticSync: !this.state.automaticSync })}
                                    value={this.state.automaticSync}
                                />
                            </Right>
                        </ListItem>
                    </View>
                </View>



            </SafeAreaView >
        )
    }
}


// styles

const styles = StyleSheet.create({
    safeView: {
        flex: 1,
        paddingTop: 24

    },
    container: {
        flex: 1,
    },

    bodyContainer: {
        marginTop: 50
    },

    text: {
        fontSize: 33,
        color: '#000000',
        alignSelf: "center",
        marginBottom: 40
    },




})


export default SettingScreen