import React from 'react';

// library
import { View, StyleSheet, SafeAreaView, Image, ScrollView } from 'react-native';


// components
import { Card } from '../../components'


class DashboardScreen extends React.Component {

    constructor() {
        super()
        this.state = {
            screen: "Dashboard Screen"
        }
    }


    render() {
        return (
            <SafeAreaView style={styles.safeView}>
                <View style={styles.logoContainer}>
                    <Image
                        source={require("../../../assets/images/header.png")}
                    />
                </View>
                <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false} >
                    <View style={styles.sliderContainer}>
                        <Image
                            source={require("../../../assets/images/dashboard.png")}
                        />
                    </View>
                    <View style={styles.buttonContainer}>
                        <Card name="Browse farms" />
                        <Card name="Search by location " onPress={() => this.props.navigation.navigate("FormSearchLocation")} />
                        <Card name="Search by MLS Number" onPress={() => this.props.navigation.navigate("FormSearch")} />
                        <Card name="Favorites" />
                    </View>

                    <View style={styles.footerContainer}>
                        <Image
                            source={require("../../../assets/images/state.png")}
                        />
                    </View>
                </ScrollView>

            </SafeAreaView>
        )
    }
}



// styles

const styles = StyleSheet.create({
    safeView: {
        flex: 1,
        paddingTop: 24

    },
    container: {
        flex: 1,
    },
    logoContainer: {
        marginTop: 30,
        alignItems: 'center'
    },
    sliderContainer: {
        marginTop: 30,
        alignItems: 'center'
    },
    text: {
        fontSize: 33,
        color: '#000000'
    },
    loader: {
        marginTop: 50
    },
    buttonContainer: {
        marginTop: 50,
        alignItems: 'center'
    },
    footerContainer: {
        marginVertical: 50,
        alignSelf: 'center',
        backgroundColor: '#D8D8D8'
    }
})



export default DashboardScreen