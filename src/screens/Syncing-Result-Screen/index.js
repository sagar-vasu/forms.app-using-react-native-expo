import React from 'react';
import { View, Text, StyleSheet, SafeAreaView } from 'react-native';



// components
import { Button, ListItem } from '../../components'

class SyncingResultScreen extends React.Component {
    constructor() {
        super()
        this.state = {
            screen: "Syncing Loading Screen"
        }
    }


    // handleClick
    handleClick = () => {
        this.props.navigation.navigate("Dashboard")
    }

    render() {
        return (
            <SafeAreaView style={styles.safeView}>
                <View style={styles.container}>
                    <View style={styles.resultContainer}>
                        <Text style={styles.syncText}>Sync Results</Text>
                        <Text style={styles.lastSyncText}>Last Sync:  Nov 9, 11:35 pm</Text>

                    </View>
                    <View style={styles.listContainer}>
                        <ListItem iconName="home-outline" title="Properties" description="Added two new properties" />
                        <ListItem iconName="star-outline" title="Favorites" description="Update listings you have favorited" />

                    </View>

                    <View style={styles.buttonContainer}>
                        <Button name="CONTINUE" onPress={this.handleClick} />
                    </View>
                </View>

            </SafeAreaView>
        )
    }
}


// styles

const styles = StyleSheet.create({
    safeView: {
        flex: 1,
        paddingTop: 24

    },
    container: {
        flex: 1,
    },

    resultContainer: {
        marginTop: 20,
        alignItems: 'center'
    },
    syncText: {
        fontSize: 33,
        color: '#000000'
    },
    lastSyncText: {
        fontSize: 14,
        fontWeight: '200',
        marginTop: 20,
        color: '#888888'

    },

    listContainer: {
        marginTop: 70,
        padding: 10,
    },

    buttonContainer: {
        marginTop: 100,
        alignItems: 'center'
    }
})



export default SyncingResultScreen