import React from 'react';
import { View, Text, StyleSheet, SafeAreaView } from 'react-native';



// components
import { Button, Input, Header, Picker } from '../../components'



// list one items

const listOne = [
    { label: "Province", value: "Province" },
    { label: "Province demo 1", value: "Province demo 1" },
    { label: "Province demo 2", value: "Province demo 2" },
    { label: "Province demo 3", value: "Province demo 3" }
]

// list two items
const listTwo = [
    { label: "Price range", value: "Price range" },
    { label: "Price range demo 1", value: "Price range demo 1" },
    { label: "Price range demo 2", value: "Price range demo 2" },
    { label: "Price range demo 3", value: "Price range demo 3" }
]

// list three items
const listThree = [
    { label: "Farm type", value: "Farm type" },
    { label: "Farm type demo 1", value: "Farm type demo 1" },
    { label: "Farm type demo 2", value: "Farm type demo 2" },
    { label: "Farm type demo 3", value: "Farm type demo 3" }

]


class FormSearch extends React.Component {
    constructor() {
        super()
        this.state = {
            screen: "Form Search Screen"
        }
    }


    // handleClick
    handleClick = () => {
        this.props.navigation.navigate("Dashboard")
    }

    render() {
        return (
            <SafeAreaView style={styles.safeView}>
                <View>
                    <Header onClick={() => this.props.navigation.navigate("Dashboard")} />
                </View>
                <View style={styles.container}>
                    <View style={styles.formContainer}>
                        <Input placeholder="MLS #" />

                        <View style={{ marginTop: 70 }}>
                            <Picker items={listOne} />
                            <Picker items={listTwo} />
                            <Picker items={listThree} />

                        </View>
                    </View>

                    <View style={styles.buttonContainer}>
                        <Button name="SEARCH" />
                    </View>
                </View>

            </SafeAreaView>
        )
    }
}


// styles

const styles = StyleSheet.create({
    safeView: {
        flex: 1,
        paddingTop: 24

    },
    container: {
        flex: 1,
    },

    formContainer: {
        marginTop: 40,
        alignItems: 'center'
    },

    buttonContainer: {
        marginTop: 70,
        alignItems: 'center'
    }
})




export default FormSearch