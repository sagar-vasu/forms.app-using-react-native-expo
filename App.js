import React from 'react';
import { AppLoading } from 'expo';
import { Container, Text } from 'native-base';
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';

import { AppRegistry, SafeAreaView } from 'react-native';

import AppNavigation from "./src/Config/Navigation"

import { SplashScreen } from './src/screens'

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
    };
  }

  async componentDidMount() {
    await Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      ...Ionicons.font,
    });
    setTimeout(() => {
      this.setState({ isReady: true });
    }, 2000);
  }

  render() {
    if (!this.state.isReady) {
      return <SplashScreen />;
    }

    return (
      <SafeAreaView style={{ flex: 1 }}>
        <AppNavigation />
      </SafeAreaView>
    );
  }
}



AppRegistry.registerComponent('App', () => App)
